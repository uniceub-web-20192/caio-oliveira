import React from 'react';
import {Text, View, StatusBar, TextInput} from 'react-native';
import {
  Form,
  Picker,
  Icon,
  Header,
  Title,
  Body,
  Button,
  Left,
  Right,
} from 'native-base';
import * as Animatable from 'react-native-animatable';
import update from 'immutability-helper';
import Children from 'react-children-utilities';
import * as Styles from '../styles';

export default class DefaultForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      values: {},
      errors: {},
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {}
  handleChangeText = (text, item) => {
    this.setState(state => {
      return update(state, {
        values: {
          [item.id]: {
            $set: item.maskFunction ? item.maskFunction(text) : text,
          },
        },
        errors: {
          $unset: [item.id],
        },
      });
    });
  };

  handleSelectedPicker = (value, item) => {
    this.setState(state => {
      return update(state, {
        values: {
          [item.id]: {
            $set: value,
          },
        },
        errors: {
          $unset: [item.id],
        },
      });
    });
  };

  handleCardOnChange = (value, item) => {
    this.setState(state => {
      return update(state, {
        values: {
          [item.id]: {
            $set: value,
          },
        },
        errors: {
          $unset: [item.id],
        },
      });
    });
  };

  handleSubmit = () => {
    let errors = {};

    this.props.items.forEach(item => {
      let error = null;

      if (item.type === 'card') {
        if (
          item.required &&
          (!this.state.values[item.id] || !this.state.values[item.id].valid)
        ) {
          error = this.getCardError(item);
          errors[item.id] = error;
        }
      } else {
        if (item.validationFunctions) {
          if (
            this.state.values[item.id] &&
            this.state.values[item.id].length > 0
          ) {
            item.validationFunctions.forEach(func => {
              if (!func.function(this.state.values)) {
                error = func.errorMessage;
              }
            });
          }
        }

        if (
          item.required &&
          (!this.state.values[item.id] ||
            this.state.values[item.id].length === 0)
        ) {
          error = item.requiredLabel;
        }

        if (error) {errors[item.id] = error;}
      }
    });

    this.setState({errors}, () => {
      this.props.handleSubmit(
        this.state.values,
        Object.keys(this.state.errors).length === 0 ? null : this.state.errors,
      );
    });
  };

  // -------------------------------------------------------------------------//
  // Other Functions
  // -------------------------------------------------------------------------//
  getCardError(item) {
    let error = '';
    if (this.state.values[item.id]) {
      const {status} = this.state.values[item.id];
      if (status.name != 'valid') {error = item.invalidNameLabel;}
      if (status.cvc != 'valid') {error = item.invalidCvcLabel;}
      if (status.expiry != 'valid') {error = item.invalidExpiryLabel;}
      if (status.number != 'valid') {error = item.invalidNumberLabel;}
    } else {error = item.requiredLabel;}

    return error;
  }
  setFields = fields => {
    this.setState(state => {
      return update(state, {
        values: {
          $merge: {
            ...fields,
          },
        },
      });
    });
  };

  submit = () => {
    this.handleSubmit();
  };

  renderElements = () => {
    return Children.deepMap(
      this.props.children,
      function(child) {
        if (
          child &&
          child.props &&
          child.props.type &&
          child.props.type === 'submit'
        ) {
          return React.cloneElement(child, {
            onPress: this.handleSubmit,
          });
        } else {
          return child;
        }
      }.bind(this),
    );
  };

  renderInput(item, index) {
    return (
      <View key={index}>
        <Text style={style.label}>{item.label}</Text>
        <TextInput
          editable={!item.disabled}
          placeholderTextColor={Styles.COLORS.DARK_GRAY}
          style={item.disabled ? style.inputDisabled : style.input}
          placeholder={item.placeholder}
          keyboardType={item.keyboardType}
          value={this.state.values[item.id]}
          onChangeText={text => this.handleChangeText(text, item)}
          maxLength={item.maxLength}
          autoCapitalize={item.autoCapitalize ? item.autoCapitalize : 'none'}
          multiline={item.type === 'textarea'}
          secureTextEntry={item.secureText}
        />
        <Animatable.Text
          animation={this.state.errors[item.id] ? 'fadeIn' : undefined}
          duration={400}
          style={style.errorMessage}>
          {this.state.errors[item.id] ? this.state.errors[item.id] : null}
        </Animatable.Text>
      </View>
    );
  }

  renderPicker(item, index) {
    return (
      <View key={index}>
        <Text style={style.label}>{item.label}</Text>

        <Picker
          renderHeader={backAction => (
            <Header style={{backgroundColor: Styles.COLORS.SECONDARY}}>
              <StatusBar
                backgroundColor={Styles.COLORS.SECONDARY}
                barStyle="light-content"
              />
              <Left>
                <Button transparent onPress={backAction}>
                  <Text style={{color: Styles.COLORS.MENU_TEXT}}>Cancelar</Text>
                </Button>
              </Left>
              <Body style={{flex: 3}}>
                <Title style={{color: Styles.COLORS.WHITE}}>Selecione</Title>
              </Body>
              <Right />
            </Header>
          )}
          mode="dropdown"
          iosIcon={<Icon name="arrow-down" style={style.pickerText} />}
          style={style.picker}
          placeholder={'Selecionar'}
          placeholderStyle={style.pickerText}
          placeholderIconColor="#fff"
          selectedValue={this.state.values[item.id]}
          textStyle={style.pickerText}
          onValueChange={picker => this.handleSelectedPicker(picker, item)}>
          <Picker.Item value={undefined} label="Selecionar" />
          {item.options
            ? item.options.map((option, i) => {
                return (
                  <Picker.Item key={i} value={option.key} label={option.name} />
                );
              })
            : null}
        </Picker>
        <Animatable.Text
          animation={this.state.errors[item.id] ? 'fadeIn' : undefined}
          duration={400}
          style={style.errorMessage}>
          {this.state.errors[item.id] ? this.state.errors[item.id] : null}
        </Animatable.Text>
      </View>
    );
  }

  render() {
    const elements = this.renderElements();

    let items = this.props.items.map((item, index) => {
      let formItem = null;

      if (item.type === 'input' || item.type === 'textarea') {
        formItem = this.renderInput(item, index);
      } else if (item.type === 'picker') {
        formItem = this.renderPicker(item, index);
      }

      return formItem;
    });

    return (
      <Form style={[style.form, this.props.style ? this.props.style : {}]}>
        {items}
        {elements}
      </Form>
    );
  }
}

const style = Styles.createStyles({
  form: {
    width: '100%',
    paddingHorizontal: Styles.utils.normalize(15),
    marginTop: 16,
  },

  inputDisabled: {
    borderBottomWidth: 1,
    borderBottomColor: Styles.COLORS.PRIMARY,
    padding: 8,
    width: '100%',
    fontSize: 20,
    color: Styles.COLORS.DARK_GRAY,
    fontWeight: '300',
  },

  picker: {
    borderBottomWidth: 1,
    borderBottomColor: Styles.COLORS.PRIMARY,
    width: '100%',
  },

  pickerContainer: {
    backgroundColor: '#fff',
  },
  pickerText: {
    color: '#000',
    fontSize: 17,
  },

  cardLabel: {
    color: Styles.COLORS.PRIMARY,
    fontWeight: '400',
    fontSize: 18,
  },
  cardInput: {
    color: Styles.COLORS.BLACK,
    fontWeight: '400',
    fontSize: 18,
  },
});
