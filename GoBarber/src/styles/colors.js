// eslint-disable-next-line no-undef
export default (COLORS = {
  BACKGROUND_DRAWER: 'rgb(142, 202, 238)',
  PRIMARY: 'rgb(95,77,147)',
  SECONDARY: 'rgb(95,77,147)',
  BLUE: 'blue',
  DARK_BLUE: '#041E42',
  LIGHT_BLUE: '#6EB4D8',
  RED: 'red',
  WHITE: 'white',
  GRAY: '#D0D0D0',
  DARK_GRAY: 'darkgray',
  LIGHT_GRAY: '#F2F2F2',
  BLACK: 'black',
  MODAL_OVERLAY: 'rgba(0,0,0,0.4)',
  FIRST_GRADIENT: 'rgb(95,77,147)',
  SECOND_GRADIENT: 'rgb(220, 121, 134)',

  SUCCESS: '#30ad63',
  WARNING: '#f0c230',
  DANGER: '#be3a30',

  MENU_TEXT: 'white',
});
