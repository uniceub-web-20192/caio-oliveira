import {Platform, StatusBar} from 'react-native';
import {COLORS} from './index';
import {normalize, SCREEN_HEIGHT, SCREEN_WIDTH} from './utils';

// global page stype
export const global = {
  flex_1: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: COLORS.BACKGROUND,
  },
  fullScreenPercentage: {
    width: '100%',
    height: '100%',
  },
  fullScreen: {
    height: SCREEN_HEIGHT,
    width: SCREEN_WIDTH,
  },
  fullWidth: {
    width: SCREEN_WIDTH,
  },
  backBox: {
    position: 'absolute',
    top: 28,
    left: 16,
  },

  backButton: {
    width: 35,
    justifyContent: 'center',
  },

  errorLabel: {
    color: COLORS.RED,
    alignSelf: 'flex-end',
    marginTop: 4,
  },

  button: {
    backgroundColor: COLORS.PRIMARY,
    width: '100%',
    height: 50,
    borderRadius: 10,
    marginTop: 16,
    alignItems: 'center',
    justifyContent: 'center',
    // shadowOffset:{  width: 0,  height: 5,  },
    // shadowColor: 'black',
    // shadowOpacity: 0.2,
    // shadowRadius: 2
  },

  buttonText: {
    color: COLORS.WHITE,
    fontSize: 17,
    fontWeight: 'normal',
  },

  smallButton: {
    backgroundColor: COLORS.PRIMARY,
    padding: 12,
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },

  smallButtonText: {
    color: COLORS.WHITE,
    fontSize: 15,
    // fontWeight: '100'
    // fontWeight: 'bold'
  },

  outlineButton: {
    borderColor: COLORS.PRIMARY,
    // backgroundColor: 'rgba(0, 0, 0, 0)',
    borderWidth: 2,
    width: '100%',
    height: 50,
    borderRadius: 10,
    marginTop: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },

  outlineButtonText: {
    color: COLORS.PRIMARY,
    fontWeight: 'bold',
    fontSize: 16,
  },

  smallOutlineButton: {
    borderColor: COLORS.PRIMARY,
    borderWidth: 1,
    padding: 12,
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },

  smallOutlineButtonText: {
    color: COLORS.PRIMARY,
    fontSize: 15,
    // fontWeight: '400'
    // fontWeight: 'bold'
  },

  forgotButton: {
    alignItems: 'center',
    padding: 8,
    marginTop: 16,
  },

  forgotButtonText: {
    color: COLORS.PRIMARY,
    fontWeight: 'bold',
    fontSize: 16,
  },

  containerCard: {
    backgroundColor: COLORS.WHITE,
    borderRadius: 7,
    marginBottom: 20,
    paddingBottom: 20,
    shadowOffset: {width: 5, height: 5},
    shadowColor: 'black',
    shadowOpacity: 0.2,
    elevation: 5,
  },

  mtop10: {
    marginTop: 10,
  },

  mtop20: {
    marginTop: 20,
  },

  mtop40: {
    marginTop: 40,
  },

  mbot10: {
    marginBottom: 10,
  },

  mbot15: {
    marginBottom: 15,
  },

  mbot20: {
    marginBottom: 20,
  },

  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },

  errorMessage: {
    color: 'red',
    textAlign: 'right',
    height: 22,
  },

  input: {
    borderBottomWidth: 1,
    borderBottomColor: COLORS.PRIMARY,
    padding: 8,
    width: '100%',
    fontSize: 20,
    fontWeight: '300',
    color: COLORS.BLACK,
  },

  label: {
    fontSize: 18,
    color: COLORS.PRIMARY,
    paddingLeft: 8,
  },

  emptyLabel: {
    fontSize: normalize(16),
    textAlign: 'center',
    marginVertical: 16,
  },

  statusContainer: {
    alignSelf: 'center',
    backgroundColor: COLORS.LIGHT_GRAY,
    paddingHorizontal: 12,
    paddingVertical: 7,
    borderRadius: 50,
  },
  statusText: {
    fontWeight: 'bold',
    fontSize: normalize(11),
    color: COLORS.WHITE,
  },

  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  oddRow: {backgroundColor: 'rgba(200, 200, 200, 0.2)'},

  divider: {
    backgroundColor: COLORS.DARK_GRAY,
    alignSelf: 'center',
    marginVertical: 2,
    width: '80%',
  },

  h1: {
    fontSize: 28,
    textAlign: 'center',
    marginTop: 13,
    fontWeight: '100',
  },
};
