import {StyleSheet} from 'react-native';
import COLORS from './colors';
import {global} from './global';
import * as utils from './utils';

function createStyles(overrides = {}) {
  return StyleSheet.create({...global, ...overrides});
}

export {COLORS, global, utils, createStyles};
