package main

import ( "net/http"
		 "time" 
		 "fmt"
		 "log")

func main() {

	StartServer()
}

func returnUrlMethod(w http.ResponseWriter, r *http.Request) {

	all := fmt.Sprintf("%v %v", r.Method, r.URL)
	w.Write([]byte(all))
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.161:8082",
			IdleTimeout: duration, 
	}

	http.HandleFunc("/", returnUrlMethod)

	log.Print(server.ListenAndServe())
}